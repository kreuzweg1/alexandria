<?php
define('LANGNOREDIRECT', TRUE);
require("./connect.php");
require("base.inc.php");

list($category,$data_id,$filename) = preg_split('_/_',$_SERVER['PATH_INFO'],-1,PREG_SPLIT_NO_EMPTY);
$data_id = intval($data_id);
$fileondisk = ALEXFILES.'/'.$category.'/'.$data_id.'/'.$filename;

if (file_exists($fileondisk) ) {
	if ($category == 'scenario') $category = 'sce';
	$referer = $_SERVER['HTTP_REFERER'];
	list($file_id) = getrow("SELECT id FROM files WHERE category = '$category' AND data_id = '$data_id'");
	doquery("INSERT INTO filedownloads (files_id, data_id, category, accesstime, referer) VALUES ('$file_id','$data_id','$category',NOW(),'".dbesc($referer)."')");
	#header("Location: http://download.alexandria.dk/files".$_SERVER['PATH_INFO']);

	// achievements
	if ($category == 'sce') {
		award_achievement(60); // download a scenario
	}

	if ($category == 'sce' && $_SESSION['user_author_id'] ) {
		$is_author = getone("SELECT 1 FROM asrel WHERE sce_id = '$data_id' AND tit_id IN (1,4) AND aut_id = '" . $_SESSION['user_author_id'] . "'");
		if ($is_author) {
			award_achievement(85); // download own scenario
		}
	}

	// redirect
	header("Location: https://download.alexandria.dk/files".$_SERVER['PATH_INFO']);

} else {
	header("HTTP/1.1 404 Not Found");
	die("The file was not found - please contact <a href=\"mailto:peter@alexandria.dk\">peter@alexandria.dk</a>.");
}
?>
