<?php
// fill in and rename to social.php
define('FACEBOOK_APP_ID', '');
define('FACEBOOK_APP_SECRET', '');

define('TWITTER_KEY', '');
define('TWITTER_SECRET', '');

define('STEAM_WEB_API_KEY', '');

define('TWITCH_CLIENT_ID', '');
define('TWITCH_CLIENT_SECRET', '');

define('DISCORD_CLIENT_ID', '');
define('DISCORD_CLIENT_SECRET', '');
?>
