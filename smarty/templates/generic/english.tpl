{assign var="pagetitle" value="English - Download role-playing games from Denmark"}
{include file="head.tpl"}

<div id="contenttext">

		<h2 class="pagetitle">
			Welcome til Alexandria
		</h2>

		<p style="font-weight: bold; font-style: italic;">We are translating Alexandria to other languages in these months. View the site <a href="/en/">in English</a> and check out <a href="/en/data?tag=English">the list of scenarios available in English</a>.
		</p>

		<p style="font-weight: bold;">The following information might be outdated:

		<p>
			Alexandria is an index of (pen-and-paper) role-playing games in Denmark and other Nordic countries. This includes
			several thousand authors and scenarios, and hundreds of conventions.
		</p>

		<p>
			Most of the scenarios are in Danish. However over a hundred scenarios have been written in or translated to English.
			Those available for download can be found <a href="/en/data?tag=English">in this list of scenarios</a>.
		</p>

		<p>
			 The original and outdated short list of English scenarios are listed below (as well as in the list mentioned above):
		</p>	

		<ul>
			<li><a href="#themothers">The Mothers, by Frederik Berg Østergaard</a></li>
			<li><a href="#doubt">Doubt, by Fredrik Axelzon and Tobias Wrigstad</a></li>
			<li><a href="#heartburn">Heartburn, by Jonas Ellemann</a></li>
			<li><a href="#oneplusone">1+1, by Simon James Pettitt</a></li>
			<li><a href="#andgoodriddance">... and good riddance!, by Malik Hyltoft</a></li>
			<li><a href="#memoirs">Memoirs of a Hitman, by Johannes Daa</a></li>
			<li><a href="#nowayout">No Way Out, by Jesper Stein Sandal</a></li>
			<li><a href="#lettheworldburn">Let the World Burn, by Peter Fallesen</a></li>
			<li><a href="#dulceetdecorum">Dulce et Decorum, by Troels Ken Pedersen</a></li>
			<li><a href="#theloss">The Loss, by Peter Fallesen</a></li>
			<li><a href="#mygirlssparrow">My Girl's Sparrow, by Troels Ken Pedersen</a></li>
			<li><a href="#wewerewasp">We were WASP, by Ann Kristine Eriksen</a></li>
			<li><a href="#vasenroad">Vasen Road, by Mikkel Bækgaard</a></li>
			<li><a href="#closedcourt">Closed Court, by Thomas Munkholt</a></li>
			<li><a href="#happyhour">Happy Hour, by Andrea Castellani</a></li>
			<li><a href="#deranged">Deranged, by Jeppe and Maria Bergmann Hamming</a></li>
			<li><a href="#adayattheoffice">A Day at the Office, by Piotr Duda-Dziewierz</a></li>
			<li><a href="#lethalwings">Lethal Wings, by Kamilla and Peter Brichs</a></li>
			<li><a href="#easter">Easter - a Tragedy, by Tim Slumstrup Aunkilde</a></li>
			<li><a href="#momdadsophie">Mom, Dad and Sophie, by Stefan von Lægteskov & Kristian Bach Pedersen</a></li>
		</ul>	

		<p>
			You can contact <a href="mailto:peter@alexandria.dk">Peter Brodersen</a> who is responsible for the site.
		</p>

		<hr />

		<h3 id="themothers">
			The Mothers, by Frederik Berg Østergaard
		</h3>
		<p>
			<a href="/gfx/scenarie/english/l_3202.jpg"><img src="/gfx/scenarie/english/s_3202.jpg" width="200" height="140" style="float:right;" alt="The Mothers" /></a>
			In the mothers' support group, there was no one that was too posh
			to talk about the baby's progress, family life, womb and sex life.
			<br><br>
			A game of fake smiles, the evil of the real world and back-stabbing.
		</p>

		<ul>
			<li><a href="download/scenario/3202/TheMothers.zip">Download "The Mothers"</a></li>
			<li><a href="data?scenarie=3202">Entry in Danish</a></li>
		</ul>	

		<hr />

		<h3 id="doubt">
			Doubt, by Fredrik Axelzon and Tobias Wrigstad
		</h3>
		<p>
			<img src="/gfx/scenarie/english/s_3198.jpg" width="180" height="180" style="float:right;" alt="Doubt" />
		A serious story of love. About how one glance can stop time. About daring to love and daring to move on.
		<br><br>
		Doubt is two stories about each other. A life and a play. Tom and Julia love each other. Both on stage and off stage.
		<br><br>
		Doubt is about temptation, the importance to love and be loved. About constant choosing. About living with one person, and at the same time dreaming about others.
		<br><br>
		In Doubt, the players are responsible for the story. Decide the fate of Tom and Julia. Play the play to its final act. Two players play Tom and two play Julia. And extras. And lovers.
		<br><br>
		A beautiful scenario about love.
		</p>

		<ul>
			<li><a href="download/scenario/3198/doubt.zip">Download "Doubt"</a></li>
			<li><a href="data?scenarie=3198">Entry in Danish</a></li>
		</ul>	

		<hr />

		<h3 id="heartburn">Heartburn, by Jonas Ellemann</h3>
		<p>
			<a href="/gfx/scenarie/english/l_3508.jpg"><img src="/gfx/scenarie/english/s_3508.jpg" width="141" height="200" style="float:right;" alt="Heartburn" /></a>
			Heartburn is about love – the romantic kind, however not the kind you find in chivalric knight-like middle-age romantic love, but the romantic love that you can find in e.g. romantic Hollywood romantic comedies or in the works of authors like Danielle Steele.
			<br><br>
			Despite its obvious shortcomings it is a widespread concept.
			<br><br>
			These two games question this concept of love.
		</p>	

		<ul>
			<li><a href="download/scenario/3508/heartburn.zip">Download "Heartburn"</a></li>
			<li><a href="data?scenarie=3508">Entry in Danish</a></li>
		</ul>	
		
		<hr />

		<h3 id="oneplusone">1+1, by Simon James Pettitt</h3>
		<p>
			<a href="/gfx/scenarie/english/l_3563.jpg"><img src="/gfx/scenarie/english/s_3563.jpg" width="141" height="200" style="float:right;" alt="1+1" /></a>
			They Meet,<br>
			Start a relationship,<br>
			Love,<br>
			Fight,<br>
			Whisper in the darkness,<br>
			And it ends in silence.
			<br><br>
			A scenario about love and relationships.
			<br><br>
			A Scenario in six predetermined scenes each played in a different way.
			<br><br>
			One Scene is played as normal table top, one is only narrated, one consists entirely of two monologs, one is played as LARP, one is set in darkness and the last is played in silence.
			The players play the scenario several times, but each time they create a new story within the same sixscenes.
			<br><br>
			Only this lose framework is predetermined and the players create the relationships and stories, everything from time, place, gender and conflicts, via improvisation and role-play.
		</p>

		<ul>
			<li><a href="download/scenario/3563/11-2players-eng.pdf">Download "1+1" (for 2 players)</a></li>
			<li><a href="download/scenario/3563/11-3players-eng.pdf">Download "1+1" (for 3 players)</a></li>
			<li><a href="data?scenarie=3563">Entry in Danish</a></li>
		</ul>	

		<hr />
		
		<h3 id="andgoodriddance">... and good riddance!, by Malik Hyltoft</h3>
		<p>
			Where did everybody go? Why not you?
			<br><br>
			You are alone with your own thoughts - locked up in your own head with no one to talk to, no one to share your life with and far too much world to go around. Your only companions are your memories of a past that makes little sense and your aspirations for a future that holds less promise.
			<br><br>
			... and good riddance is not a story about survival. It is emphatically about being alone. Only one character is in play at a time, all other players play inner voices or participate in flash backs.
			<br><br>
			The story is written within the system Apocalyze, which will be available to players prior to the game. Knowledge of the system is not imperative for participation.
		</p>

		<ul>
			<li><a href="download/scenario/3438/AndGoodRiddance.zip">Download "... and good riddance!"</a></li>
			<li><a href="data?scenarie=3438">Entry</a></li>
		</ul>	

		<hr />

		<h3 id="memoirs">Memoirs of a Hitman, by Johannes Daa</h3>
		<p>
			<a href="/gfx/scenarie/l_3544.jpg"><img src="/gfx/scenarie/s_3544.jpg" width="140" height="200" style="float:right;" alt="Memoirs of a Hitman" /></a>
			The old priest looks at me through the fog. I know he is not real. He died so long ago in a country that no longer exists. Chances are I will be joining him soon. The idea of me dying here is quite surreal. This godforsaken island filled with rocks, ice, and tall people with unpronounceable names do not really strike me as that kind of place. But hell, apparently even the best can be wrong from time to time. The faces of the people I have killed should be hunting me, but all I can think of is that old priest. And how much I fucking hate Iceland.
			<br><br>
			“Memoirs of a Hitman” is a story in three parts about a man’s development from an innocent young soldier into a merciless hitman – and about his attempt to reconnect with that youthful innocence. It is a tale of how a man lost sight of who he was in the midst of the horrors of war, only to rediscover himself in the rugged and beautiful nature of Iceland. In terms of mood, the game is close to movies like The American and Munich.
			<br><br>
			Players should expect a story-centered game where roles continuously shift hands, and with its emphasis on the story rather than immersion in the characters. That said there will be plenty of emotional scenes and tragic aspects to explore. Simple mechanics work to keep the game focused on the general theme.
			<br><br>
			Duration of play: 4-5 hours<br>
			For four players<br>
			Genre: Drama with tragic elements<br>
			Type of player: Players who want to tell stories and immerse themselves into the narrative.<br>
			Type of game master: Instructor/facilitator rather than co-player. You will be setting the scenes, and keeping the themes and the narrative in check.
		</p>

		<ul>
			<li><a href="download/scenario/3544/moah_scenario_text.pdf">Download "Memoirs of a Hitman" (scenario)</a></li>
			<li><a href="download/scenario/3544/moah_characters.pdf">Download "Memoirs of a Hitman" (characters)</a></li>
			<li><a href="data?scenarie=3544">Entry in Danish</a></li>
		</ul>	

		<hr />

		<h3 id="nowayout">No Way Out, by Jesper Sandal</h3>
		<p>
			<a href="/gfx/scenarie/english/l_3567.jpg"><img src="/gfx/scenarie/english/s_3567.jpg" width="200" height="141" style="float:right;" alt="No Way Out" /></a>
			<i>»Tails, we'll try the flooded corridor to the west. Heads, and we'll climb into the chasm to the east.«</i>
			<br><br>
			No Way Out is the story of an epic dungeon crawl that went wrong. The adventurers were lowered into the super dungeon of Undermountain beneath Waterdeep hoping that they would find riches, honor, fame, or their freedom. But what should have been a simple mission has turned into a nightmare that drives the adventurers deeper into the darkness.  Mentally as well as literally.
			<br><br>
			None of them knows exactly how long they have wandered in the dark, or where they are. They live off what they can scavenge, while they fight to stay alive against monsters, traps, and each other.
			<br><br>
			Down there in the darkness, each of them has made terrible choices. Soon they must all face the consequences of their actions, and as a player you will lead the adventurers through the last rooms of the dungeon before the final deliverance. It is a group of souls with each their secrets, wanting to be brought out into the light. This will spark suspicion, lust, shame, sacrifice, and betrayal.
			<br><br>
			As a player you will take an active part in creating the story within the framework of the simple game mechanics, which will set the pace of the game and stir the conflicts between the characters. The characters are what drive the action forward, and drama and emotional tension are a part of it from the start.
			<br><br>
			You don't need any knowledge of a particular game setting or rules system, but you are more likely to enjoy the game if you have at one time in your gaming career tried a dungeon crawl.
			<br><br>
			Timeframe: Approx. 2 hours<br>
			Number of Players: 5<br>
			Genre: Fantasy/Drama<br>
			Players: Part drama queen with a flair for scheming. You can immerse yourself in the part and at the same time take responsibility for carrying the story forward.<br>
			Game Masters: Referee with a taste for mindfucks.<br>
		</p>

		<ul>
			<li><a href="download/scenario/3567/No%20Way%20Out%20-%20the%20game.pdf">Download "No Way Out" (scenario)</a></li>
			<li><a href="download/scenario/3567/No%20Way%20Out%20-%20characters.pdf">Download "No Way Out" (characters)</a></li>
			<li><a href="download/scenario/3567/No%20Way%20Out%20-%20handouts%20EN.pdf">Download "No Way Out" (handouts)</a></li>
			<li><a href="download/scenario/3567/ingenvejud.mp3">Download ambient soundtrack</a></li>
			<li><a href="data?scenarie=3567">Entry in Danish</a></li>
		</ul>	

		<hr />

		<h3 id="lettheworldburn">Let the World Burn, by Peter Fallesen</h3>

		<p>
		<i>Dear C,
		<br><br>
		She is gone. I don’ know where she is at. The emptiness gnaws at my insides like a cancer. I have to find her. You can’t stop me, so don’t try. Only this is certain: Everything is fluid now. Brother, you will never see me again.
		<br><br>
		Love,<br>
		P.E.</i>
		</p>

		<p>
		P.E. has risked it all to find Q. Reality itself is at stake. The foundation of the world is slowly crumbling. The city smolders beneath his feet. His heart is at the breaking point.
		</p>

		<p>
		“Let the World Burn” is the story of how a young man’s quest for love leads to the world’s destruction. In the game the players will build a world and slowly let love’s destructive powers bring it down. It is a poetic tale examining love within a strange but still recognizable present, where one person’s defiance ends up tipping the scales. The gap between the knowledge of the player and the knowledge of the character drives the immersion, because the Woman is not real but the characters will never realize that. The players will play P.E, the best friend O.D., the dead brother C., what is left of Love, and the creating power of Destruction.
		</p>

		<p>
		Playtime: 4-5 hours<br>
		Number of players: 5<br>
		Genre: Dystopian love story<br>
		Player type: Players who want to immerse in both a character and a world.<br>
		Game master type: The GM shall create lingering descriptions and guide the players through the narrative and the mechanics.
		</p>

		<ul>
			<li><a href="download/scenario/3644/LettheWorldBurn.pdf">Download "Let the World Burn"</a></li>
			<li><a href="data?scenarie=3644">Entry in Danish</a></li>
		</ul>	

		<hr />

		<h3 id="dulceetdecorum">Dulce et Decorum, by Troels Ken Pedersen</h3>

		<p>
			<a href="/gfx/scenarie/l_3743.jpg"><img src="/gfx/scenarie/s_3743.jpg" width="145" height="200" style="float:right;" alt="Dulce et Decorum" /></a>
		<i>Dulce et decorum est pro patria mori</i>
		<br><br>
		“It is sweet and fitting to die for your country”
		<br>
		-Horace
		</p>

		<p>
		For very different reasons, three young English soldiers and a nurse have volunteered for service on the Western Front during World War I. They cope with the horror of war through friendship, and will come away forever changed – or die. This game explores belief and doubt, and meaningful connection in the face of death, set against a backdrop of barbed wire, gas masks, and broken hearts.
		</p>

		<p>
		<i>Dulce et decorum</i> uses ensemble play, storytelling and story game techniques to support immersive character drama. Through collaborative storytelling, the group builds up the reality of war and life on the front and uses that to pressure the characters. A simple mechanical system shapes and follows the fiction. The players wield the tools of the game – cold facts and authentic trench poetry.
		</p>

		<p>
		Duration: About 4½ hours<br>
		Number of players: Four plus one game master<br>
		Genre: War tragedy<br>
		Player type: You want to invest in both the fiction and your character, and enjoy a degree of uncertainty delivered by the game mechanics. And you think that English poems about poison gas attacks and doomed youth sound lovely.<br>
		Game master type: You love the players, and enjoy both pushing them and helping them support and push each other. Mud and barbed wire roll off your tongue like silk, and you can be a little mean.
		</p>

		<ul>
			<li><a href="download/scenario/3743/DeDalmost.pdf">Download "Dulce et Decorum" (scenario)</a></li>
			<li><a href="download/scenario/3743/DeDinaset.pdf">Download "Dulce et Decorum" (set list)</a></li>
			<li><a href="download/scenario/3743/poems_ill.pdf">Download "Dulce et Decorum" (poems)</a></li>
			<li><a href="data?scenarie=3743">Entry in Danish</a></li>
		</ul>	

		<hr />

		<h3 id="theloss">The Loss, by Peter Fallesen</h3>

		<p>
		<i>The printer wakes, and one by one, ejects the thesis pages. She stretches, stands up and walks behind him. He smiles, puts the vestment away, and follows her into the bedroom.
		<br><br>
		Seven years later, soft jazz streams from the stereo. The door to the nursery is closed. Morten and Lene try to ignore the empty high chair and eat their dinner in silence. </i>
		</p>

		<p>
		“The Loss” explores sorrow, what it means to lose oneself, and the losses that cut you in ways that words cannot describe. It also examines how to move through sorrow to the other side. The narrative permits the unsaid to remain unsaid, and plays out largely in the players’ heads, while the underlying physical game mechanics help the game to put real weight on the players’ shoulders, since sometimes emotional pain feels like a punch to the gut.
		</p>

		<p>
		Duration: 2 hours<br>
		Number of players: Four<br>
		Genre: Domestic tragedy with a small light at the end of the tunnel<br>
		Player type: You enjoy continuing the story in your head while gut-punched with emotion, and sometimes, playing abstract entities. The game mechanics entail some physical elements, but do so safely.<br>
		Game master type: The game master is both director and redeemer. You will set and cut scenes, pace the players, and try to help Morten and Lene rise from the ashes of their loss like a phoenix.
		</p>

		<ul>
			<li><a href="download/scenario/3762/TheLoss_US.pdf">Download "The Loss"</a></li>
			<li><a href="data?scenarie=3762">Entry in Danish</a></li>
		</ul>	

		<hr />

		<h3 id="mygirlssparrow">My Girl's Sparrow, by Troels Ken Pedersen</h3>
		<p>
			<a href="/gfx/scenarie/english/l_3632.jpg"><img src="/gfx/scenarie/english/s_3632.jpg" width="141" height="200" style="float:right;" alt="My Girl's Sparrow" /></a>
		<i>In a not so distant future where physical touch is going out of fashion, a small group of people
meet to be old fashioned. To touch and be touched, to seek the kind of connection that you cannot
make through even the most intricate simulations. </i>
		<br><br>
		My Girl’s Sparrow is a quiet science fiction drama about sex, love and alienation. Two couples and a widow isolate themselves in a house for a few days to swap partners and explore themselves and each other. What’s special about this game is that it's primary mode of expressing characters and exploring the situation is through roleplaying sex. Sex isn’t an indeterminate mass – it's details matter. What we do together and how we react to each other says a lot about who we are.
		<br><br>
y Girl’s Sparrow is not a pornographic scenario, though it will probably get the blood rolling in your veins, because there are serious emotions on the line for the characters.
		<br><br>
y Girl’s Sparrow isn’t an emotional extreme sport. It's not about abuse. It’s about little but crucial things like loneliness, love and desperate hope.
		<br><br>
y Girl’s Sparrow is a game about confused, alienated and amazing people.
		</p>

		<p>
		Number of players: Four to five<br>
		</p>

		<ul>
			<li><a href="download/scenario/3632/MyGirlsSparrow.pdf">Download "My Girl's Sparrow" (scenario)</a></li>
			<li><a href="download/scenario/3632/MGScharacters.pdf">Download "My Girl's Sparrow" (set list)</a></li>
			<li><a href="download/scenario/3632/MGSpoems.pdf">Download "My Girl's Sparrow" (poems)</a></li>
			<li><a href="data?scenarie=3632">Entry in Danish</a></li>
		</ul>	

		<hr />

		<h3 id="wewerewasp">We Were WASP, by Ann Kristine Eriksen</h3>
		<p>
			<a href="/gfx/scenarie/english/l_3832.jpg"><img src="/gfx/scenarie/english/s_3832.jpg" width="141" height="200" style="float:right;" alt="We were WASP" /></a>
		December 1944. A B-29 Superfortress bomber flies towards an air force base in Arizona. The plane’s further destination is unkown to its pilots. Four women sit in the cockpit. For the last year and a half, they have sacrificed themselves for a nation at war.
		<br><br>
		August 1943. The same four women meet in a barrack in Texas. To the outside world, they are unknown, to the war, they are unwanted, and to the US army they are a necessary evil. They will each endure insults, distrust, and ridicule. Through the war, they will find kindred spirits in each other, and together they will be invincible. No matter what else might occur, they will always have their memories of the plane. The unfathomable, tactile sense of flying as the only refuge of absolute freedom. High-flying, anxiety provoking, discouraging, untamable, deepfelt, wild. Friends in arms, always. Resistance is something to overcome. Life is, until it ends.
		</p>

		<ul>
			<li><a href="/download/scenario/3832/WWW%20-%20english.pdf">Download "We were WASP"</a></li>
			<li><a href="/data?scenarie=3832">Entry in Danish</a></li>
		</ul>

		<hr />

		<h3 id="vasenroad">Vasen Road, by Mikkel Bækgaard</h3>
		<p>
			<a href="/gfx/scenarie/english/l_3505.jpg"><img src="/gfx/scenarie/english/s_3505.jpg" width="141" height="200" style="float:right;" alt="Vasen Road" /></a>
		<b>Vasen no. 30, Jammerbugt county, near the Limfjord, Denmark.</b>
		<br><br>
		Zenia is dead, but nobody knows that - maybe she doesn’t even really know it herself. She is only 13 years old. The unspeakable has happened out here. Day after day goes by, and nobody intervenes.
		<br><br>
		She wants help. She screams, but nobody hears her. Nobody except for a retired social worker who for the first time in her life makes contact with that which lies beyond.
		<br><br>
		We’re in the reclaimed land surrounding the fjord. On the vase, as the old dam is called. Out here one can find the cheapest houses in the area, where the prop- erties are filled with car wrecks, broken-down dinghies, and eel traps entangled in orange fishing buoys.
		<br><br>
		Vasen Road is a realistic ghost story for four players and one Game Master. It plays out in short individual scenes, with the threads connecting each scene being up to the players. The players will play a number of roles inside of, and relating to, a family where the most horrible events have taken place. These oc- currences will never actually be described in detail.
		<br><br>
		Vasen Road is inspired by real-life horrors in Tønder, Mou, Amstetten and other rural locales where unspeakable acts are committed behind closed curtains, far from the attention of the authorities.
		</p>

		<ul>
			<li><a href="/download/scenario/3505/vasen%20road.pdf">Download "Vasen Road"</a></li>
			<li><a href="/data?scenarie=3505">Entry in Danish</a></li>
		</ul>

		<hr />

		<h3 id="closedcourt">Closed Court, by Thomas Munkholt</h3>
		<p>
			<a href="/gfx/scenarie/english/l_27.jpg"><img src="/gfx/scenarie/english/s_27.jpg" width="141" height="200" style="float:right;" alt="Closed Court" /></a>
		In the old part of town stands a foreboding brownstone building, looking like a monument founded on authority and discipline. This is my Department. And yours.
		<br><br>
		Above the enormous doors something is written in Latin about loyalty to the State – I am sure you know it by heart. You should. Crossing the wet cobbles and  going up the wide stairs towards the doors and its oath of allegiance you get the sense that the building is leaning in on you, and the Classical reliefs above the  leaded windows are fixing their accusing glare.
		<br><br>
		Grab the bronze handle, pause for a brief moment and indulge the instinct to look over your shoulder – knowing full well that you are being watched – and gaze up at the Palace. But why so self-aware? A guilty conscience, perhaps? Maybe even something to hide?
		<br><br>
		Worry not. Please, step inside. Tonight, I shall hear your confession. There shall be an inquiry in the service of truth and State. No one is without blame. Step inside, meet the others – they are waiting. Set aside your fears, come meet your destiny and let me lock these doors behind you
		</p>

		<ul>
			<li><a href="/download/scenario/27/ClosedCourt.pdf">Download "Closed Court"</a></li>
			<li><a href="/data?scenarie=27">Entry in Danish</a></li>
		</ul>

		<hr />

		<h3 id="happyhour">Happy Hour, by Andrea Castellani</h3>
		<p>
		Happy Hour is an improv-larp for eight players, adhering to the <a href="http://fate.laiv.org/dogme99/">Dogma 99 Manifesto</a>. This larp has been created along the lines of 13 at the table , the first Dogma larp and maybe the simplest larp ever.
		<br><br>
		Every player receives a firm's organigramme, where he/she will find all the characters' names, jobs and years spent working for the firm, along with the firm's organizational chart. Then every player chooses one of the ten ID badges (so there will be two badges left), where he/she will find his/her character's name and job. At the beginning only these informations are available; all the rest is improvised from scratch during the after-work drink which makes the larp's setting. Simply, an information becomes true in the moment when a character mentions it.
		<br><br>
		Have fun! Accept the other players' ideas, and develope them! Say yes! There are no limits in this improvisation, except for the names, the jobs, the years in service, and the organigramme. Nothing you can say or do is wrong. It's sufficient to avoid entering contradictory informations; should it happen anyway, the first information entered will be accepted by everybody as "the truth", while the second will be nothing more than gossip.
		</p>

		<ul>
			<li><a href="/download/scenario/3423/happyhour-english.zip">Download "Happy Hour"</a></li>
			<li><a href="/data?scenarie=3423">Entry in Danish</a></li>
		</ul>

		<hr />

		<h3 id="deranged">Deranged, by Jeppe and Maria Bergmann Hamming</h3>
		<p>
			<a href="/gfx/scenarie/english/l_4080.jpg"><img src="/gfx/scenarie/english/s_4080.jpg" width="141" height="200" style="float:right;" alt="Deranged" /></a>
Plagued by melancholy, madness, and mercury poisoning the aging composer Robert looks back on the series of mad events that tied his life so close together with the child prodigy Clara, the playful Felix, and the hopeful Johannes. How uncompromising musicality drove them from one extreme to the next — through the grand cities of Europe, prestigious parties, and to the verge of suicide.
			<br><br>
Caught between reality and dreams, Robert, on his deathbed, sees the double bars — La fin in the symphony of his life — and contemplates how everything would turn out if there was time for a Da capo.
			<br><br>
Deranged is played out through the landmark events of the composers Clara Wiecks’, Robert Schumann’s, Felix Mendelssohn’s, and Johannes Brahms’ closely interwoven lives as seen through the memories of the aging and insane Schumann. On his deathbed, he relives his life by setting the scenes and through his participation in them, lives out what actually happened and how it would have been if things had turned out differently. The scenario is a drama about the conflicts in four composers’ lives and the choices they must make between recognition, artistic integrity, love, lust, and family.
			<br><br>
Deranged is for players who want to be engulfed by a continuous flow of scenes about love, madness, and genius, which they create together on a foundation of beautiful romantic music — the music that permeated the lives of our main characters and, therefore, also this scenario. It is structured as a piece of chamber music with a series of movements that frames the dramatic course. Music is used to cut the scenes thereby allowing an uninterrupted style of play, which will engulf and seduce you.
			<br><br>
Player type: You want to achieve immersion through music combined with experimental dramatic effects and to explore the nature of memories and the boundaries between genius and madness. You do not need any knowledge of romantic music or the historical period.

		</p>
		<ul>
			<li><a href="/download/scenario/4080/Deranged.pdf">Download "Deranged"</a></li>
			<li><a href="/data?scenarie=4080">Entry in Danish</a></li>
		</ul>

		<hr style="clear: both;"  />

		<h3 id="adayattheoffice">A Day at the Office, by Piotr Duda-Dziewierz</h3>
		<p>
			<a href="/gfx/scenarie/english/l_3744.jpg"><img src="/gfx/scenarie/english/s_3744.jpg" width="200" height="150" style="float:right;" alt="A Day at the Office" /></a>
			Welcome to the office. A world filled with computers, anonymous meeting rooms, post-its and broken dreams. A place where ambition comes to die, and where apathy rules with a lazy hand.
			<br><br>
			5 employees in a medium sized IT company are stuck in their careers, but something is about to happen.
			<br><br>
			A new project is stirring and threatens to upset the old office. The battle is about to begin. Will the employees overcome the apathy and stand up to the challenge? And will they stand together or separately?
			<br><br>
			The stakes are great. After the project a much more dangerous beast lurks, its many unholy names whispered softly on the fluorescent-lit hallways: competence analysis, outsourcing, downsizing - and the most vulgar of all: layoffs.
			<br><br>
			So much is at stake, for even though life at the office can be harsh, it's nothing compared to the despair you find in the desert of unemployment, where caseworkers torture the damned with their horrible paragraph whips.
			<br><br>
			Shiver at the thought, dear reader, and come inside.
			<br><br>
			The office awaits.
		</p>
		<ul>
			<li><a href="/download/scenario/3744/A%20Day%20at%20the%20Office%20%5BENG%5D.pdf">Download "A Day at the Office"</a></li>
			<li><a href="/data?scenarie=3744">Entry in Danish</a></li>
		</ul>
		<hr style="clear: both;"  />

		<h3 id="lethalwings">Lethal Wings, by Kamilla and Peter Brichs</h3>
		<p>
			<a href="/gfx/scenarie/english/l_4079.jpg"><img src="/gfx/scenarie/english/s_4079.jpg" width="141" height="200" style="float:right;" alt="Lethal Wings" /></a>
				Storks are known for delivering the goods, when a mommy and a daddy are deeply in love, and want a newborn baby of their own. But when you need to deliver the offspring to the natural enemies of the stork, not just anyone will do. That is why we have the elite unit S.T.O.R.K. (Squad for Transfer, Oversight and Relocation of Kinfolk), which is the focal point of this scenario.
			<br><br>
				Arnold Storkenegger is a living legend for every single member of S.T.O.R.K., but during his final mission before retirement, something went wrong. He was tasked with delivering a baby elephant near the murky Limpopo River, but now he hasn’t been heard from in days. 
			<br><br>
				The mean general Al Ligator is the ruler of a banana republic, situated on the river banks of the Limpopo River. Al and his men are heavily armed – and they’ve been having issues with S.T.O.R.K. for as long as anyone can remember. That’s why the higher-ups assume that Al Ligator has kidnapped Arnold Storkenegger.
			<br><br>
				What S.T.O.R.K. doesn’t know is that the area around the Limpopo River is in the middle of a conflict: Croc O’Dile is the leader of a rebel army, wanting to overthrow General Al.
			<br><br>
				One thing is certain: S.T.O.R.K. will not accept the loss of an agent. That’s why they send a team to save Arnold Storkenegger – and hopefully deliver the baby elephant as well. 

			</p>
			<ul>
				<li><a href="/download/scenario/4079/Lethal%20Wings.pdf">Download "Lethal Wings"</a></li>
				<li><a href="/data?scenarie=4079">Entry in Danish</a></li>
		</ul>

		<hr style="clear: both;"  />

		<h3 id="easter">Easter - a Tragedy, by Tim Slumstrup Aunkilde</h3>
		<p>
			<a href="/gfx/scenarie/english/l_3565.jpg"><img src="/gfx/scenarie/english/s_3565.jpg" width="141" height="200" style="float:right;" alt="Easter - a Tragedy" /></a>
Maria starred at the paté. She had no idea what to say. How on earth would she manage to stand there and appear perfectly happy while she could feel her entire world crumple around her? Mostly she felt like standing up, raise her glass and just start to scream. Scream as long and as high as she possibly could. She wanted to throw all her loathing and all her anger against him right in his face. She wanted to tell him how she really felt towards him. She wanted to tell them all what Tim had done. Tell them what she herself had done and show them all what a living hell her life had become because of them. She would try not to cry while she would attack them with her fury and disgust, but she knew it would be impossible. She would start to cry just like mom always used to. Dad would go berserk, and she would probably never see Tim again. 
			<br><br>
			Maria moved her eyes from the paté towards her family. They were all anxiously awaiting her speech. Maria stood up, raised her glass took a deep breath while eating her rage and despair, if only short while.  
			<br><br>
			“Here is my speech about all the great and joyous things that happened to me during the last year.”  
			<br><br>
			The Hansen family is not a happy family. 
			<br><br>
			The scenario is about a small family that has secrets. The family meets once a year at Easter. It’s a story about four unhappy people who desperately tries to keep their family together. It’s a story about traditions. And it’s a story about keeping a positive mood at all times and at any price to avoid losing it all. 
			<br><br>
			<em>Disclaimer: Some mentioned characters in this scenario can seem caricatured and seem to portray people of certain genders, sex, religion or creed in a negative light. This should not be confused with the authors own conviction. But sometimes people are just unlucky and find one of the few people that enforce an unwanted prejudice. </em>
			</p>
			<ul>
				<li><a href="/download/scenario/3565/Easter%20a%20Tragedy.pdf">Download "Easter - a Tragedy"</a></li>
				<li><a href="/data?scenarie=3565">Entry in Danish</a></li>
		</ul>

		<hr style="clear: both;"  />

		<h3 id="momdadsophie">Mom, Dad and Sophie, by Stefan von Lægteskov & Kristian Bach Pedersen</h3>
		<p>
			<a href="/gfx/scenarie/english/l_3733.jpg"><img src="/gfx/scenarie/english/s_3733.jpg" width="141" height="200" style="float:right;" alt="Mom, Dad and Sophie" /></a>
			Sophie is about 12 years old and her parents have decided to get a divorce. Sophie doesn’t know why, because they never really talks to her about it. She has heard about parents who split up and remain friends. That is not the case with her parents.
			<br><br>
				They pretend that is how it is, but it really isn’t.
			<br><br>
				Both parents pull her in their own direction. They have a firm grip on both Sophie’s arms. Sometimes she thinks that they are doing it because they love her very much and want to be with her. But mainly it seems like they pull because they do not want the other one to have her.
			<br><br>
				It’s not a lot of fun being Sophie. It is not a lot of fun being Sophie’s parents either.
			<br><br>
				This is a gamemasterless freeform scenario for three players. It is about divorce, egoism and the struggle to keep yourself together. In the game you can win the divorce, but what have you done to win it?
			
			</p>
			<ul>
				<li><a href="/download/scenario/3733/Mom%20Dad%20and%20Sophie.pdf">Download "Mom, Dad and Sophie"</a></li>
				<li><a href="/data?scenarie=3733">Entry in Danish</a></li>
		</ul>

</div>

{include file="end.tpl"}
