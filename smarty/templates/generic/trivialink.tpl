{if $trivia}
<h3 class="parttitle">{$_trivia|ucfirst}</h3>
<ul class="indatalist">
{$trivia}
</ul>
{/if}

{if $link}
<h3 class="parttitle">{$_links|ucfirst}</h3>
<ul class="indatalist">
{$link}
</ul>
{/if}

